//
//  PersonInfoCell.swift
//  YueYTProject
//
//  Created by mingke on 2021/2/10.
//

import UIKit

class PersonInfoCell: UITableViewCell {

    var iconLabel : UIImageView?;
    var itemlabel : UILabel?;
    var arrowLabel : UIImageView?;
    var imagesList : [String]?;
    var namesList : [String]?;
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier);
        self.namesList = ["登陆/注册", "完善信息", "我的订单", "我的关注", "相册", "收藏", "钱包"];
        
        self.imagesList = ["../icons.bundle/customer@2x.png",  "../icons.bundle/edit@2x.png", "../icons.bundle/dayDoc@2x.png", "../icons.bundle/like@2x.png", "../icons.bundle/pic@2x.png", "../icons.bundle/collect@2x.png", "../icons.bundle/money@2x.png"];
        self.iconLabel = UIImageView.init(frame: CGRect.init(x: 10, y: 15, width: 30, height: 30));
        self.itemlabel = UILabel.init(frame: CGRect.init(x: 55, y: 10, width: 180, height: 40));
        self.itemlabel?.font = UIFont.systemFont(ofSize: 20);
        self.arrowLabel = UIImageView.init(frame: CGRect.init(x: 350, y: 10, width: 30, height: 30));
        self.addSubview(self.iconLabel!);
        self.addSubview(self.itemlabel!);
        self.addSubview(self.arrowLabel!);
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder);
    }
    
    func fillRowContent(index : Int) {
        self.iconLabel?.image = UIImage.init(named: self.imagesList![index]);
        self.itemlabel?.text = self.namesList![index];
        self.arrowLabel?.image = UIImage.init(named: "../icons.bundle/arrow@3x.png");
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
