//
//  PayBillViewController.swift
//  YueYTProject
//
//  Created by mingke on 2021/2/16.
//

import UIKit
import WebKit

class PayBillViewController: UIViewController {
    
    /*
     * "out_trade_no":"11",
     "subject":"南屏接口停车场收费账单",
     "total_amount":"45.78",
     "body":"付费金额：45.78元"
     */
    var billEntity : BillEntity?;
    var contentView : UIView?;
    var webView : WKWebView?;
    var submitButton : UIButton?;
    var orderNo : UILabel?;
    var orderSubject : UILabel?;
    var orderAmount : UILabel?;
    var orderBody : UILabel?;
    
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil);
        self.contentView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height-100));
        self.contentView?.backgroundColor = UIColor.lightGray
        self.view.addSubview(self.contentView!);
        self.navigationItem.title = "账单确认页";
        
        self.submitButton = UIButton.init(type: UIButton.ButtonType.roundedRect);
        self.submitButton?.frame = CGRect.init(x: 280, y: 820, width: 100, height: 30);
        self.submitButton?.backgroundColor = UIColor.lightGray;
        self.submitButton?.setTitle("提交", for: UIControl.State.normal);
        self.submitButton?.setTitle("提交...", for: UIControl.State.highlighted);
        self.submitButton?.showsTouchWhenHighlighted = false;
        let submitTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(submitTaphandle));
        self.submitButton?.addGestureRecognizer(submitTapRecognizer);
        self.view.addSubview(self.submitButton!);
        //展示订单详情
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder);
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white;
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let id = self.billEntity?.billId;
        self.navigationItem.title = "支付账单-\(id ?? 0)";
    }
    
    func fillBillEntity(bill : BillEntity) {
        self.billEntity = bill;
    }
    
    @objc func submitTaphandle() {
        print("我想去支付订单");
        let payVo = PayVo();
        let no = self.billEntity?.billId;
        let fee = self.billEntity?.totleFee;
        payVo.out_trade_no = "\(no ?? 0)";
        payVo.total_amount = "\(fee ?? 0)";
        payVo.subject = self.billEntity?.billParkingName ?? "" + "停车费用账单";
        payVo.body = "付款金额为：\(fee ?? 0)元";
        //发起请求
        let path = "/bill/payBillByVo";
        var param = [String:AnyObject]();
        param["out_trade_no"] = payVo.out_trade_no as AnyObject?;
        param["total_amount"] = payVo.total_amount as AnyObject?;
        param["subject"] = payVo.subject as AnyObject?;
        param["body"] = payVo.body as AnyObject?;
        AFNetTool.shared.post(path: path, params: param) { (success, data) in
            if success {
                let statu : Int = data["code"] as! Int;
                //let msg : String = data["msg"] as! String;
                if statu == 0 {
                    let htmlStr = data["result"] as! String;
                    let prefix = "<html><head><meta charset=\"utf-8\" /><title>支付页面</title></head><body>";
                    let tailfix = "</body></html>";
                    let fullHtmlStr = prefix + htmlStr + tailfix;
                    print(fullHtmlStr);
                    self.webView = WKWebView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height-100));
                    self.webView?.backgroundColor = UIColor.lightGray;
                    self.view.addSubview(self.webView!);
                    //let bundleUrl = NSURL.fileURL(withPath: Bundle.main.bundlePath);
                    //let test = "<html><head><meta charset='utf-8'/><title>测试</title></head><body>"+htmlStr+"</body></html>";
                    self.webView?.loadHTMLString(fullHtmlStr, baseURL: nil);
                }
                
            }
        }
        /*
        AFNetTool.shared.post(path: path, params: param) { (success, data) in
            //print(data);
            if success {
                let htmlStr = data["result"] as! String;
                let prefix = "<html><head><meta charset='utf-8' /><title>支付页面</title></head><body>";
                let tailfix = "</body></html>";
                let fullHtmlStr = prefix + htmlStr + tailfix;
                self.webView = WKWebView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height-100));
                self.view.addSubview(self.webView!);
                let bundleUrl = NSURL.fileURL(withPath: Bundle.main.bundlePath);
                let test = "<html><head><meta charset='utf-8'/><title>测试</title></head><body>"+htmlStr+"</body></html>";
                self.webView?.loadHTMLString(test, baseURL: bundleUrl);
            }
        }
        */
    }

}
