//
//  LoginViewController.swift
//  YueYTProject
//
//  Created by mingke on 2021/2/15.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    var backView : UIView?
    var phoneField : UITextField?;
    var codeField : UITextField?;
    var getCodeButton : UIButton?;
    var submitButton : UIButton?;
    let userDefault = UserDefaults();
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil);
        self.navigationItem.title = "手机号验证登陆";
        self.view.backgroundColor = UIColor.white;
        self.backView = UIView.init(frame: CGRect.init(x: 0, y: 250, width: self.view.bounds.width, height: 300));
        self.backView?.backgroundColor = UIColor.yellow;
        self.view.addSubview(self.backView!);
        self.phoneField = UITextField.init(frame: CGRect.init(x: (self.view.bounds.width / 2) - 100, y: 60, width: 200, height: 30));
        self.phoneField?.backgroundColor = UIColor.white;
        //设置键盘类型为数字
        //self.phoneField?.keyboardType = .numberPad;
        self.phoneField?.placeholder = "请输入手机号码";
        self.backView?.addSubview(self.phoneField!);
        self.codeField = UITextField.init(frame: CGRect.init(x: (self.view.bounds.width / 2) - 100, y: 120, width: 100, height: 30));
        self.codeField?.backgroundColor = UIColor.white;
        self.codeField?.placeholder = "请输入验证码";
        self.backView?.addSubview(self.codeField!);
        //登陆按钮
        self.submitButton = UIButton.init(type: UIButton.ButtonType.roundedRect);
        self.submitButton?.frame = CGRect.init(x: (self.view.bounds.width / 2) - 50, y: 180, width: 100, height: 30);
        self.submitButton?.backgroundColor = UIColor.lightGray;
        self.submitButton?.setTitle("提交", for: UIControl.State.normal);
        self.submitButton?.setTitle("提交...", for: UIControl.State.highlighted);
        self.submitButton?.showsTouchWhenHighlighted = false;
        let submitTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(submitTaphandle));
        self.submitButton?.addGestureRecognizer(submitTapRecognizer);
        self.backView?.addSubview(self.submitButton!);
        //获取验证码的按钮
        self.getCodeButton = UIButton.init(type: UIButton.ButtonType.roundedRect);
        self.getCodeButton?.frame = CGRect.init(x: (self.view.bounds.width / 2) + 30, y: 120, width: 70, height: 30);
        self.getCodeButton?.backgroundColor = UIColor.lightGray;
        self.getCodeButton?.setTitle("获取", for: UIControl.State.normal);
        self.getCodeButton?.setTitle("获取...", for: UIControl.State.highlighted);
        let codeTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(codeTaphandle));
        self.getCodeButton?.addGestureRecognizer(codeTapRecognizer);
        self.backView?.addSubview(self.getCodeButton!);
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder);
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //self.view.backgroundColor = UIColor.red;
    }
    
    @objc func codeTaphandle() {
        print("获取验证码");
        if let phone : String = self.phoneField?.text {
            let path = "/customer/getMessage";
            var param = [String: AnyObject]();
            param["phone"] = phone as AnyObject;
            AFNetTool.shared.get(path: path, params: param) { (success, data) in
                if success {
                    print("验证码已发送");
                }
            }
            return ;
        }
        print("手机号有误");
    }
    
    @objc func submitTaphandle() {
        if let phone : String = self.phoneField?.text, let code : String = self.codeField?.text {
            let path = "/customer/validationCode";
            var param = [String:AnyObject]();
            param["phone"] = phone as AnyObject;
            param["code"] = code as AnyObject;
            AFNetTool.shared.post(path: path, params: param) { (success, data) in
                if success {
                    print("登录成功");
                    let statu : Int = data["code"] as! Int;
                    let msg : String = data["msg"] as! String;
                    if statu == 0 {
                        print("登陆成功");
                        //存储用户id到公共存储
                        let userkey : Int = data["userkey"] as! Int;
                        self.userDefault.setValue(userkey, forKey: "userkey");
                        self.navigationController?.popViewController(animated: true);
                    }else {
                        print(msg);
                    }
                }else {
                    print(data);
                }
            }
            
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        //按下return收起键盘
        textField.resignFirstResponder();
        return true;
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        //开启键盘
        textField.becomeFirstResponder();
    }
}
