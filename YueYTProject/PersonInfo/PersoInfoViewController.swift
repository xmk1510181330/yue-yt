//
//  PersoInfoViewController.swift
//  YueYTProject
//
//  Created by mingke on 2021/2/7.
//

import UIKit

class PersoInfoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var tableView : UITableView?;
    let userDefault = UserDefaults();
    var billInfos : [BillEntity] = [];
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil);
        self.tabBarItem.title = "我的";
        self.tabBarItem.image = UIImage(named: "../icons.bundle/customer@2x.png");
        self.tabBarItem.selectedImage = UIImage(named: "../icons.bundle/customer_selected@2x.png");
        self.view.backgroundColor = UIColor.white;
        
        self.tableView = UITableView.init(frame: self.view.bounds);
        self.tableView?.dataSource = self;
        self.tableView?.delegate = self;
        
        let logo = UIImage.init(named: "../icons.bundle/logo.jpg");
        let w = self.view.bounds.width;
        let scoal = w / (logo?.size.width)!;
        let h = (logo?.size.height)! * scoal;
        let logoView = UIImageView.init(frame: CGRect.init(x: 0, y: 88, width: w, height: h));
        logoView.image = logo;
        self.tableView?.tableHeaderView = logoView;
        self.tableView?.tableFooterView = UIView.init(frame: CGRect.zero);
        self.view.addSubview(self.tableView!);
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder);
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.lightGray;
        print("personInfo");
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 7;
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = PersonInfoCell();
        //填充每一行的样式
        cell.fillRowContent(index: indexPath.row);
        return cell;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 60;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let i = indexPath.row;
        print(i);
        if i == 0 {
            //登陆
            let login = LoginViewController();
            self.navigationController?.pushViewController(login, animated: true);
        }else if i == 1 {
            //完善信息
            print("完善信息");
        }else if i == 2 {
            //查看账单
            print("查看账单");
            getBillbyUser();
        }else{
            print("other");
        }
    }
    
    func getBillbyUser() {
        self.view.showLoading();
        if let id = self.userDefault.object(forKey: "userkey") {
            let uid = id as! Int;
            print("用户id：%d", uid);
            //网络请求
            let path = "/bill/getBillListByUid";
            var param = [String:AnyObject]();
            param["id"] = uid as AnyObject;
            AFNetTool.shared.get(path: path, params: param) { (success, data) in
                if success {
                    let statu : Int = data["code"] as! Int;
                    //let msg : String = data["msg"] as! String;
                    if statu == 0 {
                        let items = data["data"] as! [NSDictionary];
                        var bills : [BillEntity] = [];
                        for i in items {
                            let bill = self.parkEntityConvert(item: i);
                            bills.append(bill);
                        }
                        self.billInfos = bills;
                        let billView = BillViewController();
                        billView.fillBillinfos(infos: self.billInfos);
                        self.view.hideLoading();
                        self.navigationController?.pushViewController(billView, animated: true);
                    }else {
                        print("获取失败");
                    }
                }
            }
        }else {
            self.view.hideLoading();
            //登陆
            let login = LoginViewController();
            self.navigationController?.pushViewController(login, animated: true);
        }
        
    }
    
    func parkEntityConvert(item : NSDictionary) -> BillEntity {
        let bill = BillEntity();
        bill.billId = item.object(forKey: "billId") as? Int;
        bill.billParkingName = item.object(forKey: "billParkName") as? String;
        bill.billStatu = item.object(forKey: "billStatu") as? Int;
        bill.totleFee = item.object(forKey: "billFee") as? Double;
        bill.timeStr = item.object(forKey: "totleTime") as? String;
        return bill;
    }
    
    /*
     {
                 "billId": 4,
                 "billCustomerId": 1,
                 "billLicenseId": 21,
                 "billParkingId": 3,
                 "billParkName": "珠海市香洲区大前门停车场",
                 "billFee": 48.0,
                 "billStatu": 102,
                 "billStart": "2021-02-10T00:44:09.000+0000",
                 "billEnd": "2021-02-10T04:23:19.000+0000",
                 "totleTime": "0天3小时39分10秒",
                 "addTime": "2021-02-10T00:44:30.000+0000",
                 "updateTime": "2021-02-17T00:44:34.000+0000",
                 "deleted": 0
             }
     */

}
