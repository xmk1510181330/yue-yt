//
//  BillListTableViewCell.swift
//  YueYTProject
//
//  Created by mingke on 2021/2/15.
//

import UIKit

class BillListTableViewCell: UITableViewCell {
    
    var parkLabel : UILabel?;
    var timeLabel : UILabel?;
    var totleFee : UILabel?;
    var statuLabel : UILabel?;
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier);
        //设置标签
        //停车场信息
        self.parkLabel = UILabel.init(frame: CGRect.init(x: 20, y: 10, width: 300, height: 30));
        self.parkLabel?.font = UIFont.systemFont(ofSize: 18);
        //self.parkLabel?.backgroundColor = UIColor.blue;
        self.contentView.addSubview(self.parkLabel!);
        //总共用时
        self.timeLabel = UILabel.init(frame: CGRect.init(x: 20, y: 50, width: 150, height: 20));
        self.timeLabel?.font = UIFont.systemFont(ofSize: 16);
        //self.timeLabel?.backgroundColor = UIColor.blue;
        self.contentView.addSubview(self.timeLabel!);
        //费用
        self.totleFee = UILabel.init(frame: CGRect.init(x: 190, y: 50, width: 100, height: 20));
        self.totleFee?.font = UIFont.systemFont(ofSize: 16);
        //self.totleFee?.backgroundColor = UIColor.blue;
        self.contentView.addSubview(self.totleFee!);
        //账单状态标签
        self.statuLabel = UILabel.init(frame: CGRect.init(x: 250, y: 50, width: 50, height: 20));
        self.statuLabel?.font = UIFont.systemFont(ofSize: 16);
        //self.statuLabel?.textColor = UIColor.red;
        //self.statuLabel?.text = "待付款";
        self.contentView.addSubview(self.statuLabel!);
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder);
    }
    
    func fillLabelContent(params : BillEntity) {
        self.parkLabel?.text = params.billParkingName;
        self.timeLabel?.text = params.timeStr;
        let fee = params.totleFee;
        self.totleFee?.text = "\(fee ?? 0.00)";
        if params.billStatu == 101 {
            //待付款
            self.statuLabel?.textColor = UIColor.red;
            self.statuLabel?.text = "待付款";
        }else if params.billStatu == 102 {
            //已付款
            self.statuLabel?.textColor = UIColor.green;
            self.statuLabel?.text = "已付款";
        }else {
            //停车计费中
            //self.statuLabel?.textColor = UIColor.red;
            self.statuLabel?.text = "计费中";
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
