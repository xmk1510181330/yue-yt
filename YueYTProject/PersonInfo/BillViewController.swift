//
//  BillViewController.swift
//  YueYTProject
//
//  Created by mingke on 2021/2/15.
//

import UIKit

class BillViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var listView : UITableView?;
    var billInfos : [BillEntity]?;
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil);
        self.view.backgroundColor = UIColor.lightGray;
        self.listView = UITableView.init(frame: self.view.bounds);
        self.listView?.dataSource = self;
        self.listView?.delegate = self;
        self.listView?.tableFooterView = UIView.init(frame: CGRect.zero);
        self.view.addSubview(self.listView!);
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder);
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "我的账单";
        //self.view.backgroundColor = UIColor.blue;
        // Do any additional setup after loading the view.
        //print("账单数量", self.billInfos?.count);
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        //print("账单数量", self.billInfos?.count)
        return self.billInfos?.count ?? 2;
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        var cell = tableView.dequeueReusableCell(withIdentifier: "id") as? BillListTableViewCell;
        if cell == nil {
            cell = BillListTableViewCell.init(style: .subtitle, reuseIdentifier: "id");
        }
        let i = indexPath.row;
        cell?.fillLabelContent(params: self.billInfos?[i] ?? BillEntity());
        return cell!;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 80;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let payBillView = PayBillViewController();
        let i = indexPath.row;
        payBillView.fillBillEntity(bill: self.billInfos?[i] ?? BillEntity());
        self.navigationController?.pushViewController(payBillView, animated: true);
    }
    
    func fillBillinfos(infos : [BillEntity]) {
        self.billInfos = infos;
    }

}
