//
//  EditorPreViewController.swift
//  YueYTProject
//
//  Created by mingke on 2021/2/8.
//

import UIKit

class EditorPreViewController: UIViewController {

    var perImage : UIImage?;
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil);
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder);
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if let img = self.perImage {
            let imgView = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: img.size.width, height: img.size.height));
            imgView.image = img;
            self.view.addSubview(imgView);
        }else{
            print("没有图片呢");
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }

    func setPreImage(image : UIImage) {
        self.perImage = image;
    }
}
