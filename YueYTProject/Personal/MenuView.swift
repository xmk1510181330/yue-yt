//
//  MenuView.swift
//  YueYTProject
//
//  Created by mingke on 2021/2/20.
//

import UIKit

class MenuView: UIView {

    var redButton : UIButton?;
    var blueButton : UIButton?;
    var blackButton : UIButton?;
    var colorType : ((_ size : Int)->Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame);
        self.backgroundColor = UIColor.lightGray;
        self.redButton = UIButton.init(frame: CGRect.init(x: 10, y: 10, width: 80, height: 40));
        self.redButton?.backgroundColor = UIColor.yellow;
        self.addSubview(self.redButton!);
        let tapRedRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapRedRequest));
        self.redButton?.addGestureRecognizer(tapRedRecognizer);
        
        self.blueButton = UIButton.init(frame: CGRect.init(x: 100, y: 10, width: 80, height: 40))
        self.blueButton?.backgroundColor = UIColor.yellow;
        self.addSubview(self.blueButton!);
        //tapBlueRequest
        let tapBlueRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapBlueRequest));
        self.blueButton?.addGestureRecognizer(tapBlueRecognizer);
        
        self.blackButton = UIButton.init(frame: CGRect.init(x: 190, y: 10, width: 80, height: 40));
        self.blackButton?.backgroundColor = UIColor.yellow;
        self.addSubview(self.blackButton!);
        //tapBlackRequest
        let tapBlackRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapBlackRequest));
        self.blackButton?.addGestureRecognizer(tapBlackRecognizer);
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder);
    }
    
    @objc func tapRedRequest(){
        print("red");
        self.colorType?(1);
    }
    
    @objc func tapBlueRequest(){
        print("blue");
        self.colorType?(2);
    }
    
    @objc func tapBlackRequest(){
        print("black");
        self.colorType?(3);
    }

}
