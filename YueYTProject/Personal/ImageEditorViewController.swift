//
//  ImageEditorViewController.swift
//  YueYTProject
//
//  Created by mingke on 2021/2/7.
//

import UIKit

class ImageEditorViewController: UIViewController, UIScrollViewDelegate {
    
    var images : [UIImage] = [];
    
    var scrollView : UIScrollView?;
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil);
        self.scrollView = UIScrollView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height-88));
        //self.scrollView?.isScrollEnabled = true;
        //self.scrollView?.contentSize =
        self.scrollView?.backgroundColor = UIColor.yellow;
        //self.scrollView?.contentSize = CGSize.init(width: self.view.bounds.width, height: 3*self.view.bounds.height);
        self.scrollView?.delegate = self;
        self.view.addSubview(self.scrollView!);
        //在最下面放置一个红色方块
        let redButton = UIView.init(frame: CGRect.init(x: 320, y: 820, width: 70, height: 40));
        redButton.backgroundColor = UIColor.red;
        self.view.addSubview(redButton);
        //给红色方块添加一个点击事件
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapRedbuttonhandle));
        redButton.addGestureRecognizer(tapRecognizer);
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder);
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "图文编辑";
        self.view.backgroundColor = UIColor.white;
        
        //initSticker();
        // Do any additional setup after loading the view.
    }
    
    func setImages(imgs : [UIImage]) {
        self.images = imgs;
    }
    
    override func viewDidAppear(_ animated: Bool) {
        initSticker();
    }
    
    func initSticker() {
        print(self.images.count);
        var ih : CGFloat = 0;
        if self.images.count > 0 {
            for image in self.images {
                //print("实际尺寸：", image.size.width, image.size.height);
                let w : CGFloat = self.scrollView!.bounds.width;
                let s = w / image.size.width;
                let h : CGFloat = image.size.height * s;
                //print("转换后的尺寸：", w, h)
                let subview = UIImageView.init(frame: CGRect.init(x: 0, y: ih, width: w, height: h));
                subview.image = image;
                
                
                self.scrollView?.addSubview(subview);
                ih  = ih + h + 8;
                //添加拖拽手势
                let panRecognizer =  UIPanGestureRecognizer(target: self, action: #selector(panImageHandle));
                subview.isUserInteractionEnabled = true;
                subview.addGestureRecognizer(panRecognizer);
                //添加旋转手势
                let rotaRecognizer = UIRotationGestureRecognizer(target: self, action: #selector(rotaHandle));
                subview.addGestureRecognizer(rotaRecognizer);
                //添加点击手势
                let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapImageHandle));
                subview.addGestureRecognizer(tapRecognizer);
                //添加缩放手势
                let pinchRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(pinchImagehandle));
                subview.addGestureRecognizer(pinchRecognizer);
            }
        }
        //加载完毕所有的子组件以后，再设置scrollView的contentSize
        self.scrollView?.contentSize = CGSize.init(width: self.view.bounds.width, height: ih);
    }
    
    @objc func panImageHandle(recognizer : UIPanGestureRecognizer) {
        //print("move");
        let imageView = recognizer.view;
        
        //选中状态下，方可移动图片
        let translation = recognizer.translation(in: imageView);
        imageView?.center = CGPoint.init(x: imageView!.center.x + translation.x, y: imageView!.center.y + translation.y);
        recognizer.setTranslation(CGPoint.zero, in: imageView!);
        
    }
    
    @objc func tapRedbuttonhandle() {
        print("点击事件");
        print(self.view.bounds.width, self.view.bounds.height);
        print(self.scrollView!.contentSize.width, self.scrollView!.contentSize.height);
        //开启一个画图上下文
        UIGraphicsBeginImageContext(self.scrollView!.contentSize);
        //开始渲染
        self.scrollView!.layer.render(in: UIGraphicsGetCurrentContext()!);
        //获取到图片
        let imageTmp = UIGraphicsGetImageFromCurrentImageContext()!;
        UIGraphicsEndImageContext();
        let preView = EditorPreViewController();
        preView.setPreImage(image: imageTmp);
        self.navigationController?.pushViewController(preView, animated: true);
    }
    
    @objc func rotaHandle(recognizer : UIRotationGestureRecognizer) {
        print("开始旋转");
    }
    
    @objc func tapImageHandle(recognizer : UITapGestureRecognizer) {
        print("点击图片激活");
        let view = recognizer.view;
        //要先获取到当前ScrollView上所有的子视图，取消其他
        if let sc = view?.superview as? UIScrollView {
            let items = sc.subviews as [UIView];
            for item in items {
                item.layer.borderWidth = 0;
            }
        }
        view?.layer.borderWidth = 3;
        view?.layer.borderColor = UIColor.red.cgColor;
        //UIColor.color(withHex: "#004FFE").cgColor;
    }
    
    @objc func pinchImagehandle(recognizer : UIPinchGestureRecognizer) {
        let view = recognizer.view;
        let w = view?.bounds.width;
        let h = view?.bounds.width;
        view?.bounds.size = CGSize.init(width: w! * recognizer.scale, height: h! * recognizer.scale);
    }

}
