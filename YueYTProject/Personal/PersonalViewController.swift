//
//  PersonalViewController.swift
//  YueYTProject
//
//  Created by mingke on 2021/2/2.
//

import UIKit
import MapKit
import CoreBluetooth


class PersonalViewController: UIViewController, UITextViewDelegate {
    
    var tvContent : UITextView?;
    var menuView : MenuView?;
    var layoutButton : UIButton?;
    var aMoudle : AMoudle?;
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil);
        self.tabBarItem.title = "我的";
        self.tabBarItem.image = UIImage(named: "../icons.bundle/customer@2x.png");
        self.tabBarItem.selectedImage = UIImage(named: "../icons.bundle/customer_selected@2x.png");
        //测试图片选择器的红色方块
        let redView = UIView.init(frame: CGRect.init(x: 100, y: 400, width: 100, height: 100));
        redView.backgroundColor = UIColor.red;
        redView.layer.shadowColor = UIColor.black.cgColor;
        redView.layer.shadowPath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44)).cgPath;
        redView.layer.shadowOffset = CGSize.init(width: 0, height: -2);
        redView.layer.shadowOpacity = 0.05;
        self.view.addSubview(redView);
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapRedViewHandle));
        redView.addGestureRecognizer(tapRecognizer);
        //测试textView
        self.tvContent = UITextView.init(frame: CGRect.init(x: 100, y: 200, width: 300, height: 100));
        self.tvContent?.backgroundColor = UIColor.white;
        self.view.addSubview(self.tvContent!);
        //添加编辑框的选中时间
        let tapTvContentRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapTvContentRequest));
        self.tvContent?.addGestureRecognizer(tapTvContentRecognizer);
        
        let label = UILabel();
        label.text = "请输入文本内容…"
        label.numberOfLines = 0
        label.font = UIFont(name: "PingFang SC", size: 14)
        label.textColor = UIColor.blue;
        label.sizeToFit()
        self.tvContent!.addSubview(label)
        self.tvContent!.setValue(label, forKeyPath: "_placeholderLabel")
        //设置文本的字体
        self.tvContent?.font = UIFont.init(name: "Snell Roundhand", size: 16);
        //设置文本的对齐方式
        self.tvContent?.textAlignment = .center;
        //设置文本的加粗
        //self.tvContent?.markedTextStyle();
        //设置文本的斜体
        
        //设置文本的下划线
        
        //设置文本的删除线
        self.tvContent?.delegate = self;
        self.menuView = MenuView.init(frame: CGRect.init(x: 0, y: 600, width: self.view.bounds.width, height: 300));
        self.menuView!.colorType = {
            [weak self] (type:Int) in
            guard let wkself = self else { return }
            //在这里，就可以拿到菜单传递过来的type改变文字
            let colors = [UIColor.red, UIColor.blue, UIColor.black];
            wkself.tvContent?.textColor = colors[type-1];
        }
        self.view.addSubview(self.menuView!);
        self.layoutButton = UIButton.init(frame: CGRect.init(x: (self.view.bounds.width/2)-40, y: 100, width: 80, height: 40));
        self.layoutButton?.backgroundColor = UIColor.blue;
        self.view.addSubview(self.layoutButton!);
        let tapBlueRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapBlueRequest));
        self.layoutButton?.addGestureRecognizer(tapBlueRecognizer);
        initKeyBoard();
        self.aMoudle = AMoudle();
    }
    
    func initKeyBoard() {
        GTKeyBoardObserver.observe(on: self) { (kbInfo) in
            print("键盘使用");
            print(kbInfo.curve);
            print(kbInfo.duration);
            print(kbInfo.isShow);
            print(kbInfo.height)
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder);
    }

    override func viewDidLoad() {
        super.viewDidLoad();
        self.view.backgroundColor = UIColor.white;
        print("person");
        //AFNetTool.shared.get(path: "/settle/list1", params: nil);
        
    }
    
    @objc func tapRedViewHandle() {
        //点击了红色方块
        print("点击了红色方块");
        self.aMoudle?.address = "河南省郑州市金水区";
        /*
        let urlStr = "<a href=\"https://www.baidu.com\"></a>";
        let afterUrl = urlStr.replacingOccurrences(of: "\"", with: "'")
        print(afterUrl);*/
        /*
        ImageAbout.shared.chooseImages(maxCnt: 6, isNeed: false) { (success, data) in
            if success {
                let images = data as [UIImage];
                //print("选择完成：%d", images.count);
                let imageEdit = ImageEditorViewController();
                imageEdit.setImages(imgs: images);
                self.navigationController?.pushViewController(imageEdit, animated: true);
            }
        }*/
        /*
        let menu = MenuView(frame: CGRect.init(x: 0, y: 600, width: self.view.bounds.width, height: 300));
        menu.colorType = {
            [weak self] (type:Int) in
            guard let wkself = self else { return }
            //在这里，就可以拿到菜单传递过来的type改变文字
            let colors = [UIColor.red, UIColor.blue, UIColor.black];
            wkself.tvContent?.textColor = colors[type-1];
        }
        self.view.addSubview(menu);
        //实现菜单里面的block
        */
        
    }
    
    /*
    func textViewDidChange(_ textView: UITextView){
        print("文本内容: ", textView.text);
    }*/
    
    @objc func tapTvContentRequest() {
        self.tvContent?.layer.borderWidth = 3;
        self.tvContent?.layer.borderColor = UIColor.red.cgColor;
        self.tvContent?.becomeFirstResponder();
        
    }
    
    @objc func tapBlueRequest() {
        print("点击了蓝色按钮");
        touchToAppleMap();
        //touchToAppleMap();
        //let next = YellowViewController();
        //self.navigationController?.pushViewController(next, animated: true);
        
        /*
        let vcArr = self.navigationController?.viewControllers.reversed() ?? [];
        for item in vcArr {
            print(item)
            if item is ParkLotViewController {
                self.navigationController?.popToViewController(item, animated: true);
            }
        }*/
    }
    //if the life has trouble, don not cry, shou shoudle traggle with them forever
    //苹果地图的导航模块，需要输入目的地的经纬度和名称
    func touchToAppleMap() {
        let lat : Double = 22.258613;
        let lng : Double = 113.58633;
        let destination : String = "珠海市香洲区海滨南路108号";
        let loc = CLLocationCoordinate2DMake(lat, lng)
        let currentLocation = MKMapItem.forCurrentLocation()
        let toLocation = MKMapItem(placemark:MKPlacemark(coordinate:loc,addressDictionary:nil))
                toLocation.name = destination
        let boo = MKMapItem.openMaps(with: [currentLocation,toLocation], launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving,MKLaunchOptionsShowsTrafficKey: NSNumber(value: true)])
        print(boo)
    }
    
    //高德地图的导航模块，需要输入应用名称，目的地经纬度和目的地的名称
    func touchToGaoDeMap() {
        let appName = "YueYT";
        let dlat : Double = 22.258613;
        let dlon : Double = 113.58633;
        let dname : String = "珠海市香洲区海滨南路108号";
        let way = 0;
        let urlString = "iosamap://path?sourceApplication=\(appName)&dname=\(dname)&dlat=\(dlat)&dlon=\(dlon)&t=\(way)" as String
                
        if self.openMap(urlString) == false {
            print("您还没有安装高德地图")
            let urlString = "itms-apps://itunes.apple.com/app/id452186370"
            self.openURL(urlString: urlString)

        }
    }
    
    // 打开第三方地图
    private func openMap(_ urlString: String) -> Bool {

         let urlstr =   urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
        guard let url = URL(string:urlstr) else {
            return false
        }
        if UIApplication.shared.canOpenURL(url) == true {
            self.openURL(urlString: urlString as String)
            return true
        } else {
            return false
        }
    }
  func openURL(urlString:String) {
        let urlstr =   urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
        
        guard let url = URL(string:urlstr) else {
            return
        }
        //根据iOS系统版本，分别处理
        if #available(iOS 10, *) {
            UIApplication.shared.open(url, options: [:]) { (success) in
                print(success);
            }
        } else {
            UIApplication.shared.openURL(url )
        }
    }
    
    // 高德经纬度转为百度地图经纬度
    // 百度经纬度转为高德经纬度，减掉相应的值就可以了。
    func getBaiDuCoordinateByGaoDeCoordinate(coordinate:CLLocationCoordinate2D) -> CLLocationCoordinate2D {
        return CLLocationCoordinate2DMake(coordinate.latitude + 0.006, coordinate.longitude + 0.0065)
    }

}
