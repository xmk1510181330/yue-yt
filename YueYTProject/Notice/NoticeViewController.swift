//
//  NoticeViewController.swift
//  YueYTProject
//
//  Created by mingke on 2021/2/2.
//

import UIKit

class NoticeViewController: UIViewController {
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil);
        self.tabBarItem.title = "通知";
        self.tabBarItem.image = UIImage(named: "../icons.bundle/notice@2x.png");
        self.tabBarItem.selectedImage = UIImage(named: "../icons.bundle/notice_selected@2x.png");
        //print("1. init");
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder);
    }

    override func viewDidLoad() {
        super.viewDidLoad();
        self.view.backgroundColor = UIColor.yellow;
        //print("2. viewDidLoad");
        
    }
    
    /*
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        print("3. viewWillAppear");
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
        print("6. viewDidAppear");
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews();
        print("4. viewWillLayoutSubviews");
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews();
        print("5. viewDidLayoutSubviews");
    }*/

}
