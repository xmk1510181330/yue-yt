//
//  PointListTableViewCell.swift
//  YueYTProject
//
//  Created by mingke on 2021/2/21.
//

import UIKit

class PointListTableViewCell: UITableViewCell {
    
    //
    var cellAction : ((Int)->Void)?;
    var pointEntity : ParkPointEntity?;
    var iconImage : UIImageView?;
    var typeLabel : UILabel?;
    var statuLabel : UILabel?;
    var addressLabel : UILabel?;
    var switchButton : UIButton?;
    var startButton = UIButton();
    let centerImage = UIImage.init(named: "../icons.bundle/center@2x.png");
    let pointImage = UIImage.init(named: "../icons.bundle/point@2x.png");
    let userDefault = UserDefaults();
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier);
        //地址，编号对应的标签
        self.addressLabel = UILabel.init(frame: CGRect.init(x: 80, y: 10, width: 220, height: 30));
        self.addressLabel?.font = UIFont.systemFont(ofSize: 18);
        //self.addressLabel?.backgroundColor = UIColor.blue;
        self.contentView.addSubview(self.addressLabel!);
        //图标标签
        self.iconImage = UIImageView(frame: CGRect.init(x: 10, y: 10, width: 60, height: 60));
        //iconImage?.backgroundColor = UIColor.blue;
        self.contentView.addSubview(self.iconImage!);
        //类型标签
        self.typeLabel = UILabel.init(frame: CGRect.init(x: 80, y: 50, width: 100, height: 20));
        self.typeLabel?.font = UIFont.systemFont(ofSize: 16);
        //self.typeLabel?.backgroundColor = UIColor.blue;
        self.contentView.addSubview(self.typeLabel!);
        //状态标签
        self.statuLabel = UILabel.init(frame: CGRect.init(x: 200, y: 50, width: 100, height: 20));
        self.statuLabel?.font = UIFont.systemFont(ofSize: 16);
        //self.statuLabel?.backgroundColor = UIColor.blue;
        self.contentView.addSubview(self.statuLabel!);
        //开关按钮
        self.switchButton = UIButton.init(type: UIButton.ButtonType.roundedRect);
        self.switchButton?.frame = CGRect.init(x: 310, y: 25, width: 100, height: 30);
        self.switchButton?.backgroundColor = UIColor.cyan;
        self.switchButton?.setTitle("使用", for: UIControl.State.normal);
        self.switchButton?.setTitle("使用...", for: UIControl.State.highlighted);
        self.switchButton?.showsTouchWhenHighlighted = false;
        let switchTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(switchTapHandle));
        self.switchButton?.addGestureRecognizer(switchTapRecognizer);
        //按钮是否展示取决于节点类型和状态
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder);
    }
    
    override func layoutSubviews() {
        super.layoutSubviews();
        
    }
    
    @objc func switchTapHandle() {
        print("点击了开关");
        let statu = self.pointEntity?.parkPointStatus;
        if let id = self.userDefault.object(forKey: "userkey") {
            let uid = id as! Int;
            print("用户id：%d", uid);
            //let id = point.parkPointId;
            //网络请求
            let path = "/parkpoint/switchPointUseStatu";
            var param = [String : AnyObject]();
            param["uid"] = uid as AnyObject;
            param["pointId"] = self.pointEntity?.parkPointId as AnyObject;
            //如果是未启用，就启用，否则车辆离开，节点变成未启用的状态
            if statu == 101 {
                param["switchStatu"] = 102 as AnyObject;
            }else{
                param["switchStatu"] = 101 as AnyObject;
            }
            AFNetTool.shared.post(path: path, params: param) { (success, data) in
                if success {
                    print("打开成功");
                    if self.cellAction != nil {
                        self.cellAction!(101);
                    }
                }
            }
        }
    }
    
    func setCellAction(_ action: @escaping ((Int) -> ())) {
        self.cellAction = action;
    }
    
    
    func fillLabelContent(point : ParkPointEntity) {
        self.pointEntity = point;
        //状态处理
        let type = point.parkPointType;
        if type == 1 {
            //中心节点
            self.typeLabel?.text = "中心节点";
            self.iconImage?.image = self.centerImage;
        }else if type == 2 {
            //工作节点
            self.typeLabel?.text = "工作节点";
            self.iconImage?.image = self.pointImage;
        }else {
            self.typeLabel?.text = "工作节点";
            self.iconImage?.image = self.pointImage;
        }
        //工作状态处理
        let statu = point.parkPointStatus;
        let customerId = point.customerId;
        if statu == 101 {
            self.statuLabel?.textColor = UIColor.green;
            self.statuLabel?.text = "节点可用";
            if type == 2 {
                self.switchButton?.backgroundColor = UIColor.cyan;
                self.switchButton?.setTitle("使用", for: UIControl.State.normal);
                self.switchButton?.setTitle("使用...", for: UIControl.State.highlighted);
                //节点可用，而且为工作节点
                self.contentView.addSubview(self.switchButton!);
            }
        }else if statu == 102 {
            self.statuLabel?.textColor = UIColor.red;
            self.statuLabel?.text = "节点已用";
            //如果节点已用，而且customerid和用户id一样，就显示按钮
            print("用户id：", customerId);
            if let id = self.userDefault.object(forKey: "userkey") {
                let uid = id as! Int;
                print("uid: \(uid)");
                if uid == customerId {
                    //self.switchButton?.frame = CGRect.init(x: 310, y: 10, width: 100, height: 30);
                    self.switchButton?.backgroundColor = UIColor.green;
                    self.switchButton?.setTitle("离开", for: UIControl.State.normal);
                    self.switchButton?.setTitle("离开...", for: UIControl.State.highlighted);
                    self.switchButton?.showsTouchWhenHighlighted = false;
                    self.contentView.addSubview(self.switchButton!);
                    //开启按钮
                    //self.startButton.frame
                }
            }
        }else {
            self.statuLabel?.textColor = UIColor.gray;
            self.statuLabel?.text = "节点掉线";
        }
        let pid = point.parkId;
        let spid : String = String(pid!);
        let id = point.parkPointId;
        let sid : String = String(id!);
        let address = point.parkPointAddress;
        //print(address!);
        self.addressLabel?.text = "\(spid)-\(sid)-硬件地址：\(address!)";
        setNeedsDisplay();
    }
    
    @objc func startPointHandler() {
        print("startPoint");
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
