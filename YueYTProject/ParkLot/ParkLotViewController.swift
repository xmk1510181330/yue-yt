//
//  ParkLotViewController.swift
//  YueYTProject
//
//  Created by mingke on 2021/2/2.
//

import UIKit
import CoreLocation

struct LocationInfo : Codable{
    var administrativeArea : String;
    var locality : String;
    var subLocality : String;
    var thoroughfare : String;
    var name : String;
}

class ParkLotViewController: UIViewController, CLLocationManagerDelegate {
    
    let locationManger = CLLocationManager();
    
    var nearparkLot : UIView!;
    var titleLabel1 : UILabel!;
    var descLabel1 : UILabel!;
    var icon1 : UIImageView!;
    
    var historyParkLot : UIView!;
    var titleLabel2 : UILabel!;
    var descLabel2 : UILabel!;
    var icon2 : UIImageView!;
    
    var parkInfos : [ParkEntity]?;
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil);
        //设置tabbar
        self.tabBarItem.title = "停车场";
        self.tabBarItem.image = UIImage(named: "../icons.bundle/park@2x.png");
        self.tabBarItem.selectedImage = UIImage(named: "../icons.bundle/park_selected@2x.png");
        
        self.nearparkLot = UIView.init(frame: CGRect.init(x: 10, y: 150, width: 190, height: 120));
        self.nearparkLot.backgroundColor = UIColor.init(red: 175/255, green: 221/255, blue: 222/255, alpha: 1);
        //附近停车场的标题标签
        self.titleLabel1 = UILabel(frame: CGRect.init(x: 10, y: 30, width: 80, height: 20));
        self.titleLabel1.font = UIFont.systemFont(ofSize: 12);
        self.titleLabel1.text = "附近停车场";
        //self.titleLabel1.backgroundColor = UIColor.blue;
        self.nearparkLot.addSubview(self.titleLabel1);
        //附近停车场的描述标签
        self.descLabel1 = UILabel.init(frame: CGRect.init(x: 10, y: 70, width: 100, height: 20));
        self.descLabel1.font = UIFont.systemFont(ofSize: 12);
        self.descLabel1.text = "根据定位推荐场地";
        //self.descLabel1.backgroundColor = UIColor.blue;
        self.nearparkLot.addSubview(self.descLabel1);
        //附近停车场的图标
        self.icon1 = UIImageView.init(frame: CGRect.init(x: 120, y: 30, width: 60, height: 60))
        self.icon1.image = UIImage(named: "../icons.bundle/nearby@2x.png");
        //self.icon1.backgroundColor = UIColor.blue;
        self.nearparkLot.addSubview(self.icon1);
        
        self.historyParkLot = UIView.init(frame: CGRect.init(x: 210, y: 150, width: 190, height: 120))
        self.historyParkLot.backgroundColor = UIColor.init(red: 175/255, green: 221/255, blue: 222/255, alpha: 1);
        //历史使用停车场的标题标签
        self.titleLabel2 = UILabel(frame: CGRect.init(x: 10, y: 30, width: 80, height: 20));
        self.titleLabel2.font = UIFont.systemFont(ofSize: 12);
        self.titleLabel2.text = "历史停车场";
        //self.titleLabel2.backgroundColor = UIColor.blue;
        self.historyParkLot.addSubview(self.titleLabel2);
        //历史停车场的描述标签
        self.descLabel2 = UILabel.init(frame: CGRect.init(x: 10, y: 70, width: 100, height: 20));
        self.descLabel2.font = UIFont.systemFont(ofSize: 12);
        self.descLabel2.text = "推荐去过的停车场";
        //self.descLabel2.backgroundColor = UIColor.blue;
        self.historyParkLot.addSubview(self.descLabel2);
        //历史停车场的图标
        self.icon2 = UIImageView.init(frame: CGRect.init(x: 120, y: 30, width: 60, height: 60))
        //self.icon2.backgroundColor = UIColor.blue;
        self.icon2.image = UIImage(named: "../icons.bundle/history@2x.png");
        self.historyParkLot.addSubview(self.icon2);
        
        //添加点击事件
        let nearClick = UITapGestureRecognizer.init(target: self, action: #selector(getParkList));
        self.nearparkLot.addGestureRecognizer(nearClick);
        
        let historyClick = UITapGestureRecognizer.init(target: self, action: #selector(historyClickhandle));
        self.historyParkLot.addGestureRecognizer(historyClick);
        
        self.view.addSubview(self.nearparkLot);
        self.view.addSubview(self.historyParkLot);
        //设置各种delegate
        self.locationManger.delegate = self;
        //设置定位进度
        self.locationManger.desiredAccuracy = kCLLocationAccuracyBest
        //更新距离
        self.locationManger.distanceFilter = 100
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        //获取最新的坐标
        let location = locations.last ?? CLLocation();
        let grocoder = CLGeocoder();
        grocoder.reverseGeocodeLocation(location) { (placemarks, error) in
            if error != nil {
                print("定位出错");
                return;
            }
            
            if let place = placemarks?[0] {
                // 国家 省  市  区  街道
                let administrativeArea = place.administrativeArea ?? ""
                let locality = place.locality ?? ""
                let subLocality = place.subLocality ?? ""
                let thoroughfare = place.thoroughfare ?? ""
                let name = place.name ?? ""
                var info = [String : AnyObject]();
                info["administrativeArea"] = administrativeArea as AnyObject;
                info["locality"] = locality as AnyObject;
                info["subLocality"] = subLocality as AnyObject;
                info["thoroughfare"] = thoroughfare as AnyObject;
                info["name"] = name as AnyObject;
                //AFNetTool.shared.post(path: "/park/getUserLocation", params: info);
            }
        }
        //关闭定位信息
        self.locationManger.stopUpdatingLocation();
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder);
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor.white;
        print("parklot");
    
    }
    
    @objc func nearClickhandle() {
        print("我想要获取位置");
        //发送授权申请
        self.locationManger.requestAlwaysAuthorization()
        if (CLLocationManager.locationServicesEnabled()){
            //允许使用定位服务的话，开启定位服务更新
            self.locationManger.startUpdatingLocation()
            print("定位开始");
        }else{
            print("请打开定位权限");
        }
        
    }
    
    @objc func historyClickhandle() {
        print("获取之前用过的停车场");
    }
    
    @objc func getParkList() {
        //获取到停车场信息并推送
        self.view.showLoading();
        print("来了")
        var locationinfo = [String : AnyObject]();
        locationinfo["administrativeArea"] = "广东省" as AnyObject;
        locationinfo["locality"] = "珠海市" as AnyObject;
        locationinfo["subLocality"] = "香洲区" as AnyObject;
        locationinfo["thoroughfare"] = "广湾街83号" as AnyObject;
        locationinfo["name"] = "珠海艾派克微电子有限公司" as AnyObject;
        
        AFNetTool.shared.post(path: "/park/getParkWithLocation", params: locationinfo) { (success, data) in
            if success {
                var parks : [ParkEntity] = [];
                let items = data["result"] as! [NSDictionary];
                for item in items {
                    let parkItem = self.convertAnyToPark(item: item);
                    //print(parkItem);
                    parks.append(parkItem);
                }
                self.parkInfos = parks;
                let listView = ParkListViewController();
                listView.fillParkList(park1s: parks);
                self.view.hideLoading();
                self.navigationController?.pushViewController(listView, animated: true);
            }else{
                self.view.hideLoading();
                print("请求失败");
                print(data);
            }
        };
    }
    
    func convertAnyToPark(item : NSDictionary) -> ParkEntity {
        let result = ParkEntity();
        result.parkName = item.object(forKey: "parkName") as? String;
        result.parkAddress = item.object(forKey: "parkAddress") as? String;
        result.parkSettleId = item.object(forKey: "parkSettleId") as? Int;
        result.parkAreaId = item.object(forKey: "parkAreaId") as? Int;
        result.parkId = item.object(forKey: "parkId") as? Int;
        result.parkInUse = item.object(forKey: "parkInUse") as? Int;
        result.parkTotle = item.object(forKey: "parkTotle") as? Int;
        result.parkSettleId = item.object(forKey: "parkSettleId") as? Int;
        result.parkLatitude = item.object(forKey: "parkLatitude") as? Double;
        result.parkLongitude = item.object(forKey: "parkLongitude") as? Double;
        return result;
    }
    /*
     addTime = "2021-02-06T03:50:14.000+0000";
     daleted = 0;
     parkAddress = "\U73e0\U6d77\U5e02\U91d1\U6e7e\U533a\U6b66\U6c49\U5357\U8def90\U53f7";
     parkAdminId = 3;
     parkAreaId = 852;
     parkId = 1;
     parkInUse = 4;
     parkLatitude = "43.67";
     parkLongitude = "78.90000000000001";
     parkName = "\U73e0\U6d77\U5e02\U91d1\U6e7e\U533a\U4e07\U79d1\U4e00\U671f\U505c\U8f66\U573a";
     parkPic = 1;
     parkSettleId = 1;
     parkTotle = 50;
     updateTime = "2021-02-06T03:50:12.000+0000";
     
     */

}
