//
//  PointListViewController.swift
//  YueYTProject
//
//  Created by mingke on 2021/2/21.
//

import UIKit

class PointListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var pointList : UITableView?;
    var parkEntity : ParkEntity?;
    var parkPoints : [ParkPointEntity]?;

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil);
        self.view.backgroundColor = UIColor.lightGray;
        self.pointList = UITableView.init(frame: self.view.bounds);
        self.pointList?.delegate = self;
        self.pointList?.dataSource = self;
        self.pointList?.tableFooterView = UIView.init(frame: CGRect.zero);
        self.view.addSubview(self.pointList!);
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder);
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //let pid = self.parkEntity?.parkId;
        //self.navigationItem.title = "停车位列表\(pid)";
        //self.view.backgroundColor = UIColor.yellow;
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    func fillParkEntity(park : ParkEntity, points : [ParkPointEntity]) {
        self.parkEntity = park;
        self.parkPoints = points;
        self.navigationItem.title = parkEntity?.parkName;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        //print("停车场：", self.parks?.count);
        return self.parkPoints?.count ?? 2;
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        var cell = tableView.dequeueReusableCell(withIdentifier: "id") as? PointListTableViewCell;
        if cell == nil {
            cell = PointListTableViewCell.init(style: .subtitle, reuseIdentifier: "id");
        }
        let i = indexPath.row;
        cell?.fillLabelContent(point: (self.parkPoints?[i] ?? ParkPointEntity()))
        //设置闭包
        cell?.setCellAction({ [weak self] (type) in
            print("闭包：", type);
            self?.reloadPointData();
        })
        return cell!;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 80;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let i = indexPath.row;
        print(i);
    }
    
    func reloadPointData() {
        self.view.showLoading();
        let path = "/parkpoint/getPointByParkLotId";
        var param = [String : AnyObject]();
        param["pid"] = self.parkEntity?.parkId as AnyObject;
        AFNetTool.shared.get(path: path, params: param) { (success, data) in
            if success {
                var points : [ParkPointEntity] = [];
                let items = data["result"] as! [NSDictionary];
                for item in items {
                    let point = self.convertAnyToPoint(item: item);
                    points.append(point);
                }
                self.parkPoints = points;
                self.pointList?.reloadData();
                self.view.hideLoading();
            }
        }
    }
    
    func convertAnyToPoint(item : NSDictionary) -> ParkPointEntity {
        let result = ParkPointEntity();
        result.parkPointId = item.object(forKey: "parkPointId") as? Int;
        result.parkId = item.object(forKey: "parkId") as? Int;
        result.parkPointAddress = item.object(forKey: "parkPointAddress") as? String;
        result.parkPointType = item.object(forKey: "parkPointType") as? Int;
        result.parkPointStatus = item.object(forKey: "parkPointStatus") as? Int;
        result.customerId = item.object(forKey: "customerId") as? Int;
        return result;
    }

}
