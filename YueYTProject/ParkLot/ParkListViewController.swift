//
//  ParkListViewController.swift
//  YueYTProject
//
//  Created by mingke on 2021/2/6.
//

import UIKit

class ParkListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var listView : UITableView?;
    
    var parks : [ParkEntity]?;
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil);
        self.navigationItem.title = "停车场信息列表";
        self.view.backgroundColor = UIColor.lightGray;
        self.listView = UITableView.init(frame: self.view.bounds);
        self.listView?.dataSource = self;
        self.listView?.delegate = self;
        self.listView?.tableFooterView = UIView.init(frame: CGRect.zero);
        self.view.addSubview(self.listView!);
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder);
    }
    
    override func viewDidLoad() {
        super.viewDidLoad();
        //print("停车场", self.parks?.count);
        // Do any additional setup after loading the view.
    }
    
    func fillParkList(park1s : [ParkEntity]) {
        self.parks = park1s;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        //print("停车场：", self.parks?.count);
        return self.parks?.count ?? 2;
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        var cell = tableView.dequeueReusableCell(withIdentifier: "id") as? ParkListTableViewCell;
        if cell == nil {
            cell = ParkListTableViewCell.init(style: .subtitle, reuseIdentifier: "id");
        }
        let i = indexPath.row;
        cell?.fillLabelContent(param: self.parks?[i] ?? ParkEntity());
        return cell!;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 80;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.showLoading();
        let i = indexPath.row;
        let parkEntity : ParkEntity = self.parks![i];
        //获取到对应停车场的节点列表
        let path = "/parkpoint/getPointByParkLotId";
        var param = [String : AnyObject]();
        param["pid"] = parkEntity.parkId as AnyObject;
        AFNetTool.shared.get(path: path, params: param) { (success, data) in
            if success {
                var points : [ParkPointEntity] = [];
                let items = data["result"] as! [NSDictionary];
                for item in items {
                    let point = self.convertAnyToPoint(item: item);
                    points.append(point);
                }
                let pointListView = PointListViewController();
                pointListView.fillParkEntity(park: parkEntity, points: points);
                self.view.hideLoading();
                self.navigationController?.pushViewController(pointListView, animated: true);
            }
        }
        
        //print(i);
    }
    
    func convertAnyToPoint(item : NSDictionary) -> ParkPointEntity {
        let result = ParkPointEntity();
        result.parkPointId = item.object(forKey: "parkPointId") as? Int;
        result.parkId = item.object(forKey: "parkId") as? Int;
        result.parkPointAddress = item.object(forKey: "parkPointAddress") as? String;
        result.parkPointType = item.object(forKey: "parkPointType") as? Int;
        result.parkPointStatus = item.object(forKey: "parkPointStatus") as? Int;
        result.customerId = item.object(forKey: "customerId") as? Int;
        return result;
    }

}
