//
//  ParkPointEntity.swift
//  YueYTProject
//
//  Created by mingke on 2021/2/21.
//

import UIKit

class ParkPointEntity: NSObject {

    var customerId : Int?;
    var parkPointId : Int?;
    var parkId : Int?;
    var parkPointAddress : String?;
    var parkPointType : Int?;
    var parkPointStatus : Int?;
    var addTime : String?;
    var updateTime : String?;
    var deleted : Int?;
}
