//
//  PayVo.swift
//  YueYTProject
//
//  Created by mingke on 2021/2/17.
//

import UIKit

class PayVo: NSObject {

    /*
     "out_trade_no":"11",
         "subject":"南屏接口停车场收费账单",
         "total_amount":"45.78",
         "body":"付费金额：45.78元"
     */
    var out_trade_no : String?;
    var subject : String?;
    var total_amount : String?;
    var body : String?;
}
