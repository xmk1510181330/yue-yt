//
//  ParkEntity.swift
//  YueYTProject
//
//  Created by mingke on 2021/2/6.
//

import UIKit

class ParkEntity: Codable {
    
    var parkId : Int?;
    var parkAreaId : Int?;
    var parkAdminId : Int?;
    var parkSettleId : Int?;
    var parkName : String?;
    var parkAddress : String?;
    var parkPic : Int?;
    var parkTotle : Int?;
    var parkInUse : Int?;
    var parkLatitude : Double?;
    var parkLongitude : Double?;
    var addTime : String?;
    var updateTime : String?;
    var daleted : Int?;

}
