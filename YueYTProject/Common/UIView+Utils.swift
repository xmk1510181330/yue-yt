//
//  UIView+Utils.swift
//  YueYTProject
//
//  Created by mingke on 2021/2/7.
//

import UIKit

extension UIView {
    
    @objc func runMain(_ block:@escaping ()->Void) {
        DispatchQueue.main.async {
            block()
        }
    }
    
    @objc func showLoading(text: String? = nil) {
        //在主线程中运行
        self.runMain {
            let hud = MBProgressHUD.showAdded(to: self, animated: true)
            hud.label.text = text
        }
    }
    
    @objc func hideLoading(_ animated: Bool = false) {
        self.runMain {
            MBProgressHUD.hide(for: self, animated: animated)
        }
    }

}
