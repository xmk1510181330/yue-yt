//
//  ImageAbout.swift
//  YueYTProject
//
//  Created by mingke on 2021/2/7.
//

import UIKit

//定义一个block方便返回数据
typealias ImagesBlock = (_ success : Bool, _ data : [UIImage]) -> Void

class ImageAbout: NSObject {
    
    static let shared = ImageAbout();
    
    func chooseImages(maxCnt : Int, isNeed : Bool, complate : @escaping ImagesBlock) {
        //使用TZPicker选择图片
        if let vc = TZImagePickerController(maxImagesCount: maxCnt, delegate: nil) {
            vc.allowTakeVideo = false;
            vc.allowPickingVideo = false;
            vc.allowCameraLocation = false;
            vc.didFinishPickingPhotosHandle = {[weak self] (images:[UIImage]?, assets: Any, isSelectOriginalPhoto:Bool)-> Void in
                guard self != nil else { return }
                if isNeed {
                    print("需要上传");
                } else {
                    complate(true, images ?? []);
                }
            }
            UIApplication.shared.keyWindow?.rootViewController?.present(vc, animated: true, completion: nil)
        }
    }
}
