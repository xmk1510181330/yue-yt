//
//  KeyBoardObserver.swift
//  YueYTProject
//
//  Created by mingke on 2021/2/22.
//

import UIKit

class KeyBoardObserver: NSObject {
    
    override init() {
        super.init();
        self.onKeyBoardhandle();
    }
    
    @objc func onKeyBoardhandle() {
        print("触发了键盘的相关事件");
    }

}
