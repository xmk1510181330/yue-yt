//
//  YueYTBleTool.swift
//  YueYTProject
//
//  Created by mingke on 2021/3/16.
//

import UIKit
import CoreBluetooth

class YueYTBleTool: NSObject {

    static let shared = YueYTBleTool();
    var centralManager : CBCentralManager!;
    var peripheralManager : CBPeripheralManager?
    var deviceList = [CBPeripheral]();
    var connectedPeripheral : CBPeripheral?;
    
    override init() {
        //蓝牙工具初始化
        super.init();
        self.initCentralManager();
        self.checkBluetooth();
    }
    
    func initCentralManager() {
        print("初始化蓝牙管理中心");
        if centralManager == nil {
            centralManager = CBCentralManager(delegate: self, queue: DispatchQueue.main, options: nil);
        }
    }
    
    func checkBluetooth() {
        print("检查蓝牙状态");
        if centralManager == nil {
            centralManager = CBCentralManager(delegate: self, queue: DispatchQueue.main, options: nil);
        }
        
        //检查蓝牙连接状态
        if centralManager.state == .poweredOff || centralManager.state == .unauthorized {
            print("蓝牙未打开，或无权限");
        }
    }
    //搜索外设
    func discoverPeripherals() {
        if centralManager == nil {
            centralManager = CBCentralManager(delegate: self, queue: DispatchQueue.main, options: nil);
        }
        
        centralManager.scanForPeripherals(withServices: nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey: true]);
    }
    //停止搜索外设
    func stopDiscover() {
        centralManager.stopScan();
    }
    //连接外设
    func connectPeripheral(peripheral : CBPeripheral) {
        
    }
    //断开外设
    func disConnectPeripheral(peripheral : CBPeripheral) {
        
    }
}

//CBCentralManagerDelegate,处理中心管理器的相关回调
extension YueYTBleTool : CBCentralManagerDelegate {
    //蓝牙状态改变的回调函数
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        print("Bletooth状态修改为：", central.state);
    }
    //发现外设
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        //print(peripheral.name ?? "BluetoothDevice", RSSI.intValue);
        //检测设备信号，有信号则添加设备进入数组
        guard peripheral.name != nil else {
            return
        }
        if RSSI.intValue < 0 && RSSI.intValue > -80 {
            print(peripheral.name ?? "BluetoothDevice", RSSI.intValue);
            deviceList.append(peripheral);
        }
    }
    //连接到外设
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("蓝牙设备连接成功");
        print(peripheral.name ?? "BluetoothDevice")
    }
    //连接失败
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print("蓝牙设备连接失败");
    }
    //断开连接
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        print("连接已断开");
    }

}

extension YueYTBleTool : CBPeripheralDelegate {
    
}
