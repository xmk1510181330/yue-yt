//
//  GTKeyBoardObserver.h
//  YueYTProject
//
//  Created by mingke on 2021/2/22.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
//引入这个类
@class KeyboardInfo;

@interface GTKeyBoardObserver : NSObject

@property (nonatomic, strong) void(^callback)(KeyboardInfo *kbinfo);

+ (instancetype)observeOn:(NSObject *)obj callback:(void(^)(KeyboardInfo* kbinfo))callback;

@end

@interface KeyboardInfo : NSObject

@property (nonatomic, assign) NSTimeInterval duration;
@property (nonatomic, assign) NSInteger curve;
@property (nonatomic, assign) NSInteger height;
@property (nonatomic, assign, getter=isShow) BOOL show;

@end

NS_ASSUME_NONNULL_END
