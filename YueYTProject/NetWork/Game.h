//
//  Game.h
//  YueYTProject
//
//  Created by mingke on 2021/2/20.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    ThisRPGGame = 0,
    ThisActionGame = 1,
    ThisBattleFlagGame = 2,
} ThisGameType;

@interface Game : NSObject
@property (copy,nonatomic) NSString *Name;
@property (assign,nonatomic) ThisGameType Type;

- (void)GiveThisGameName:(NSString *)name;
- (void)GiveThisGameType:(ThisGameType)type;
@end

NS_ASSUME_NONNULL_END
