//
//  AFNetTool.swift
//  YueYTProject
//
//  Created by mingke on 2021/2/6.
//

import UIKit
import Alamofire
import Foundation

struct RequestType {
    static let GET : String = "GET";
    static let POST : String = "POST";
}

typealias NetworkCompleteBlock = (_ success: Bool, _ data: [String:AnyObject]) -> Void

typealias NetworkCompleteBlock1 = (_ success: Bool, _ data: String) -> Void

class AFNetTool: NSObject {
    
    static let shared = AFNetTool();
    //103.46.128.53
    let baseUrl = "http://3761c948r0.wicp.vip";
    
    //get请求
    func get(path : String, params : [String:AnyObject]?, complate : @escaping NetworkCompleteBlock) {
        let realPath = baseUrl + path;
        let encoding : ParameterEncoding = URLEncoding(destination: .queryString, arrayEncoding: .noBrackets, boolEncoding: .numeric);
        let header : HTTPHeaders = [];
        AF.request(realPath, method: .get, parameters: params, encoding: encoding, headers: header, interceptor: nil, requestModifier: nil).responseJSON { (resp) in
            guard let value = resp.value as? [String : AnyObject] else{
                //请求失败
                //print("请求失败");
                return ;
            }
            //print("请求成功");
            if let status = value["status"] {
                //404, 500, 403...
                print("状态码：%@", status);
                DispatchQueue.main.async {
                    complate(false, value);
                }
                return;
            }
            let code = value["code"];
            if code is Int, code as! Int == 0 {
                //print("拿到数据了");
                //print(value);
                DispatchQueue.main.async {
                    complate(true, value);
                }
            }else{
                //let msg = value["msg"] as! String;
                //print(msg);
                DispatchQueue.main.async {
                    complate(true, value);
                }
            }
        }
    }
    
    //post请求
    func post(path : String, params : [String:AnyObject]?, complate : @escaping NetworkCompleteBlock) {
        let realPath = baseUrl + path;
        let encoding = JSONEncoding.default;
        let header : HTTPHeaders = [];
        //header.add(HTTPHeader(name: "Content-Type", value: "application/json"));
        AF.request(realPath, method: .post, parameters: params, encoding: encoding, headers: header, interceptor: nil, requestModifier: nil).responseJSON { (resp) in
            guard let value = resp.value as? [String : AnyObject] else{
                //请求失败
                //print("请求失败");
                return ;
            }
            //print("请求成功");
            if let status = value["status"] {
                //404, 500, 403...
                print("状态码：%@", status);
                DispatchQueue.main.async {
                    complate(false, value);
                }
                return;
            }
            let code = value["code"];
            if code is Int, code as! Int == 0 {
                //print("拿到数据了");
                //print(value);
                DispatchQueue.main.async {
                    complate(true, value);
                }
            }else{
                //let msg = value["msg"] as! String;
                //print(msg);
                DispatchQueue.main.async {
                    complate(true, value);
                }
            }
        }
    }
    
    //post请求
    func postStr(path : String, params : [String:AnyObject]?, complate : @escaping NetworkCompleteBlock1) {
        let realPath = baseUrl + path;
        let encoding = JSONEncoding.default;
        let header : HTTPHeaders = [];
        //header.add(HTTPHeader(name: "Content-Type", value: "application/json"));
        AF.request(realPath, method: .post, parameters: params, encoding: encoding, headers: header, interceptor: nil, requestModifier: nil).responseString{ (resp) in
            print("response is \(resp)");
            //let afterUrl = urlStr.replacingOccurrences(of: "\"", with: "'")
            //let data = resp as String;
            DispatchQueue.main.async {
                //complate(true, resp);
            }
        }
    }
    
}
